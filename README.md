[deprecated] move to [gitlab.com/tryturned](https://gitlab.com/tryturned/leetcode) and [github/wavty](https://github.com/wavty/wavty/tree/main/content/docs/Algorithm)

📣 **leetcode**

[leetcode repo](./) means go solutions to leetcode problems. It's based [labuladong algorithm](https://labuladong.gitee.io/algo/) and inspired by [doocs/leetcode](https://github.com/doocs/leetcode).

⭐ **Contributing**

Welcome to contribute together to make it better.
