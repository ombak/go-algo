package lcof

import (
	"gitee.com/ombak/leetcode/pkg/list"
)

// https://leetcode.cn/problems/lian-biao-zhong-dao-shu-di-kge-jie-dian-lcof/
func getKthFromEnd(head *list.ListNode, k int) *list.ListNode {
	p, r := head, head
	for i := 0; i < k && p != nil; i++ {
		p = p.Next
	}
	for p != nil {
		p = p.Next
		r = r.Next
	}
	return r
}
