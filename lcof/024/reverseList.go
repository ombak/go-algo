package lcof

import (
	"gitee.com/ombak/leetcode/pkg/list"
)

// https://leetcode.cn/problems/fan-zhuan-lian-biao-lcof/
func reverseList(head *list.ListNode) *list.ListNode {
	if head == nil || head.Next == nil {
		return head
	}
	last := reverseList(head.Next)
	head.Next.Next = head
	head.Next = nil
	return last
}
