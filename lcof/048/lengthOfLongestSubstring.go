package lcof

// https://leetcode.cn/problems/zui-chang-bu-han-zhong-fu-zi-fu-de-zi-zi-fu-chuan-lcof/
func lengthOfLongestSubstring(s string) int {
	need := make(map[byte]bool)
	res := 0
	for left, right := 0, 0; right < len(s); right++ {
		// shrink
		for need[s[right]] {
			need[s[left]] = false
			left++
		}
		need[s[right]] = true
		if right-left+1 > res {
			res = right - left + 1
		}
	}
	return res
}
