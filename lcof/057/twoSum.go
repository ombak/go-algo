package lcof

// https://leetcode.cn/problems/he-wei-sde-liang-ge-shu-zi-lcof/
func twoSum(nums []int, target int) []int {
	left, right := 0, len(nums)-1
	for left < right {
		sum := nums[left] + nums[right]
		if sum > target {
			right--
		} else if sum < target {
			left++
		} else {
			return []int{nums[left], nums[right]}
		}
	}
	return []int{}
}
