package lcof2

// https://leetcode.cn/problems/MPnaiL/
func checkInclusion(s1 string, s2 string) bool {
	need, window := make(map[byte]int), make(map[byte]int)
	for i := range s1 {
		need[s1[i]] += 1
	}
	for validate, left, right := 0, 0, 0; right < len(s2); right++ {
		c := s2[right]
		window[c] += 1
		if need[c] == window[c] {
			validate++
		}
		// shrink
		for right-left+1 >= len(s1) {
			if validate == len(need) {
				return true
			}
			d := s2[left]
			left++
			if need[d] == window[d] {
				validate--
			}
			window[d] -= 1
		}
	}
	return false
}
