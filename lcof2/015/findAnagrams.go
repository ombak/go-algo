package lcof2

// https://leetcode.cn/problems/VabMRr/
func findAnagrams(s string, p string) []int {
	res, need, window := make([]int, 0), make(map[byte]int), make(map[byte]int)
	for i := range p {
		need[p[i]] += 1
	}
	for valid, left, right := 0, 0, 0; right < len(s); right++ {
		c := s[right]
		window[c] += 1
		if need[c] == window[c] {
			valid++
		}
		// shrink
		for right-left+1 >= len(p) {
			if valid == len(need) {
				res = append(res, left)
			}
			d := s[left]
			left++
			if window[d] == need[d] {
				valid--
			}
			window[d] -= 1
		}
	}
	return res
}
