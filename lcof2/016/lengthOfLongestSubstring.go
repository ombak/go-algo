package lcof2

// https://leetcode.cn/problems/wtcaE1/
func lengthOfLongestSubstring(s string) int {
	need := make(map[byte]bool)
	res := 0
	for left, right := 0, 0; right < len(s); right++ {
		for need[s[right]] {
			need[s[left]], left = false, left+1
		}
		need[s[right]] = true
		if right-left+1 > res {
			res = right - left + 1
		}
	}
	return res
}
