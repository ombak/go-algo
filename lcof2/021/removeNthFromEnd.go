package lcof2

import (
	"gitee.com/ombak/leetcode/pkg/list"
)

// https://leetcode.cn/problems/SLwz0R/
func removeNthFromEnd(head *list.ListNode, n int) *list.ListNode {
	dummy := &list.ListNode{Next: head}
	pre, now := dummy, dummy
	for i := 0; i < n && pre != nil; i++ {
		pre = pre.Next
	}
	// note:
	// pre.Next != nil is (n+1)th node from end
	// pre != nil is nth node from end
	for pre.Next != nil {
		pre = pre.Next
		now = now.Next
	}
	now.Next = now.Next.Next
	return dummy.Next
}
