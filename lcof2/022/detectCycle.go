package lcof2

import (
	"gitee.com/ombak/leetcode/pkg/list"
)

// https://leetcode.cn/problems/c32eOV/
func detectCycle(head *list.ListNode) *list.ListNode {
	f, s := head, head
	for f != nil && f.Next != nil {
		f = f.Next.Next
		s = s.Next
		if f == s {
			break
		}
	}
	if f == nil || f.Next == nil {
		return nil
	}
	for head != f {
		head = head.Next
		f = f.Next
	}
	return f
}
