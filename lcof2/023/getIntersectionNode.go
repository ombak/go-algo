package lcof2

import (
	"gitee.com/ombak/leetcode/pkg/list"
)

// https://leetcode.cn/problems/3u1WK4/
func getIntersectionNode(headA, headB *list.ListNode) *list.ListNode {
	d1, d2 := headA, headB
	for d1 != d2 {
		if d1 == nil {
			d1 = headB
		} else {
			d1 = d1.Next
		}
		if d2 == nil {
			d2 = headA
		} else {
			d2 = d2.Next
		}
	}
	return d1
}
