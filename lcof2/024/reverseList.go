package lcof2

import (
	"gitee.com/ombak/leetcode/pkg/list"
)

// https://leetcode.cn/problems/UHnkqh/
func reverseList(head *list.ListNode) *list.ListNode {
	if head == nil || head.Next == nil {
		return head
	}
	pre, now := head, head.Next
	pre.Next = nil
	for now != nil {
		nt := now.Next
		now.Next = pre
		pre = now
		now = nt
	}
	return pre
}
