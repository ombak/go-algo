package leetcode

// https://leetcode.com/problems/longest-substring-without-repeating-characters/
func lengthOfLongestSubstring(s string) int {
	exists := make([]bool, 128)
	length := 0
	for i, j := 0, 0; j < len(s); j++ {
		for ; exists[s[j]]; i++ {
			exists[s[i]] = false
		}
		exists[s[j]] = true
		if length < j-i+1 {
			length = j - i + 1
		}
	}
	return length
}
