package leetcode

import (
	"fmt"
	"testing"
)

func TestLongestPalindrome(t *testing.T) {
	s := "122221678"
	// fmt.Println(LongestPalindromeByExpand(s))
	fmt.Println(LongestPalindromeByDynamic(s))
}
