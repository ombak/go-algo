package leetcode

import (
	"gitee.com/ombak/leetcode/pkg/list"
)

// https://leetcode.com/problems/remove-nth-node-from-end-of-list/
func removeNthFromEnd(head *list.ListNode, n int) *list.ListNode {
	dummy := &list.ListNode{Next: head}
	f, s := dummy, dummy
	for i := 0; i < n; i++ {
		f = f.Next
	}
	for f != nil && f.Next != nil {
		f = f.Next
		s = s.Next
	}
	s.Next = s.Next.Next
	return dummy.Next
}
