package leetcode

import (
	"fmt"
	"math/rand"
	"testing"
	"time"

	"gitee.com/ombak/leetcode/pkg/list"
)

func TestRemoveNthFromEnd(t *testing.T) {
	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	l := list.NewRandomList()
	n := r.Intn(l.Length()) + 1
	fmt.Println(n)
	fmt.Println(l.Println())
	fmt.Println(removeNthFromEnd(l, n).Println())
}
