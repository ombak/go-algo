package leetcode

import (
	"gitee.com/ombak/leetcode/pkg/list"
)

// https://leetcode.com/problems/merge-two-sorted-lists/
func mergeTwoLists(list1 *list.ListNode, list2 *list.ListNode) *list.ListNode {
	dummy := &list.ListNode{}
	p := dummy
	for list1 != nil && list2 != nil {
		if list1.Val <= list2.Val {
			p.Next = list1
			list1 = list1.Next
		} else {
			p.Next = list2
			list2 = list2.Next
		}
		p = p.Next
	}
	if list1 != nil {
		p.Next = list1
	}
	if list2 != nil {
		p.Next = list2
	}
	return dummy.Next
}
