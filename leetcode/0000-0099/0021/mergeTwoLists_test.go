package leetcode

import (
	"fmt"
	"testing"

	"gitee.com/ombak/leetcode/pkg/list"
)

func TestMergeTwoLists(t *testing.T) {
	l1, l2 := list.NewRandomList(), list.NewRandomList()
	r := mergeTwoLists(l1, l2)
	fmt.Println(r.Println())
}
