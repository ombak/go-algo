package leetcode

import (
	"fmt"
	"testing"

	"gitee.com/ombak/leetcode/pkg/list"
)

func TestMergeKLists(t *testing.T) {
	l1, l2 := list.NewRandomList(), list.NewRandomList()
	arg := make([]*list.ListNode, 2)
	arg = append(arg, l1, l2)
	fmt.Printf("l1.length + l2.length = %d, ", l1.Length()+l2.Length())
	r := mergeKLists2(arg)
	fmt.Printf("r.len = %d.\n", r.Length())
}
