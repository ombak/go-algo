package leetcode

// https://leetcode.com/problems/remove-duplicates-from-sorted-array/
func removeDuplicates(nums []int) int {
	l, r := 0, 0
	for ; r < len(nums); r++ {
		if nums[r] > nums[l] {
			l++
			nums[l] = nums[r]
		}
	}
	return l + 1
}
