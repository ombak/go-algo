package leetcode

// https://leetcode.com/problems/remove-element/
func removeElement(nums []int, val int) int {
	l, r := 0, 0
	for ; r < len(nums); r++ {
		if nums[r] != val {
			nums[l] = nums[r]
			l++
		}
	}
	return l
}
