package leetcode

import "gitee.com/ombak/leetcode/pkg/list"

// https://leetcode.com/problems/remove-duplicates-from-sorted-list/
func DeleteDuplicates(head *list.ListNode) *list.ListNode {
	if head == nil {
		return head
	}
	left, right := head, head
	p := left
	for right != nil {
		if right.Val > p.Val {
			p.Next = right
			p = p.Next
		}
		right = right.Next
	}
	p.Next = nil
	return left
}
