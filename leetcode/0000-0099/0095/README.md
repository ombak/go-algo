[Unique Binary Search Trees II](https://leetcode.com/problems/unique-binary-search-trees-ii/)

**方法一：递归**

和[0096](../0096/)同样的思路，暴力穷举 1 ～ n 之间的每一个数字 i 为 root 节点时的所有可能构造情况。当 i 为 root 节点时，其左子树构成值范围为[1,i-1]，右子树构成值范围为[i+1,n]。需要注意的是本题由于需要使用到确切的值，不能像[0096](../0096/)一样直接使用 n 做为参数，需要扩展函数使用参数限定取值范围。

```go
/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
func generateTrees(n int) []*TreeNode {
	return build(1, n)
}

func build(low, high int) []*TreeNode {
	if low > high {
		return []*TreeNode{nil}
	}
	res := make([]*TreeNode, 0)
	for i := low; i <= high; i++ {
		left, right := build(low, i-1), build(i+1, high)
		for _, lnode := range left {
			for _, rnode := range right {
				root := &TreeNode{Val: i, Left: lnode, Right: rnode}
				res = append(res, root)
			}
		}
	}
	return res
}
```
