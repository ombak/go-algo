[Unique Binary Search Trees](https://leetcode.com/problems/unique-binary-search-trees/)

**方法一：递归**

暴力解法，以 1 ～ n 之间的每一个数字 i 作为 BST 的根节点，则每种情况下 BST 的数目为[1,i-1]和[i+1,n]两个列表构成的 BST 数目的乘积。很容易知道当 n=0 或者 n=1 是只能是空子树或者单节点的二叉树，情况一定，所以可直接返回结果值为 1。

```go
func numTrees(n int) int {
    // 使用 memo 数组消除重复子问题
	memo := make([]int, n+1)
	memo[0], memo[1] = 1, 1
	var count func(n int) int
	count = func(n int) int {
		if memo[n] != 0 {
			return memo[n]
		}
		res := 0
		for i := 1; i <= n; i++ {
			left, right := count(i-1), count(n-i)
			res += left * right
		}
		memo[n] = res
		return res
	}
	return count(n)
}
```

**方法二：动态规划**

即方法一的迭代实现。

```go
func numTrees(n int) int {
	memo := make([]int, n+1)
	memo[0], memo[1] = 1, 1

	for i := 2; i <= n; i++ {
		for j := 0; j < i; j++ {
			memo[i] += memo[j] * memo[i-j-1]
		}
	}
	return memo[n]
}
```
