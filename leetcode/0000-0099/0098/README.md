[Validate Binary Search Tree](https://leetcode.com/problems/validate-binary-search-tree/)

**方法一：中序遍历**

使用二叉搜索树中序遍历是递增序列的特性，确保在中序遍历的过程中每一个节点的值均比前一个节点的值大即可。

```go
/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
func isValidBST(root *TreeNode) bool {
	preNode := math.MinInt64
	var traversal func(root *TreeNode) bool
	traversal = func(root *TreeNode) bool {
		if root == nil {
			return true
		}
		left := traversal(root.Left)
		if root.Val <= preNode {
			return false
		}
		preNode = root.Val
		right := traversal(root.Right)
		return left && right
	}
	return traversal(root)
}
```

**方法二：递归**

```go
/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
func isValidBST(root *TreeNode) bool {
	minInt, maxInt := math.MinInt, math.MaxInt
	var traversal func(root *TreeNode, min, max int) bool
	traversal = func(root *TreeNode, min, max int) bool {
		if root == nil {
			return true
		}
		if root.Val <= min || root.Val >= max {
			return false
		}
		return traversal(root.Left, min, root.Val) && traversal(root.Right, root.Val, max)
	}
	return traversal(root, minInt, maxInt)
}
```
