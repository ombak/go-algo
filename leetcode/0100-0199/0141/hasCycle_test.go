package leetcode

import (
	"fmt"
	"log"
	"testing"

	"gitee.com/ombak/leetcode/pkg/list"
)

func TestHasCycle(t *testing.T) {
	// test no cycle singly linked list
	for i := 0; i < 10; i++ {
		linked := list.NewRandomList()
		if hasCycle(linked) {
			fmt.Println(linked.Println())
			log.Fatalln("NewRandomList hasCycle case error: ")
		}
	}
	fmt.Println("no cycle test success")
	// test cycle singly linked list
	for i := 0; i < 10; i++ {
		linked := list.NewRandomCycleList()
		if !hasCycle(linked) {
			fmt.Println(linked.Println())
			log.Fatalln("NewRandomCycleList  case error: ")
		}
	}
	fmt.Println("cycle test success")
}
