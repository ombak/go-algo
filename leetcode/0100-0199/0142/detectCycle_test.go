package leetcode

import (
	"fmt"
	"testing"

	"gitee.com/ombak/leetcode/pkg/list"
)

func TestDetectCycle(t *testing.T) {
	// no cycle list
	nocycle := list.NewRandomList()
	fmt.Println(nocycle.Println())
	fmt.Println(detectCycle(nocycle))
	// cycle list
	cycle := list.NewRandomCycleList()
	fmt.Println(detectCycle(cycle))
}
