package leetcode

// Definition for a binary tree node.
type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

// https://leetcode.com/problems/binary-tree-preorder-traversal/
func preorderTraversal(root *TreeNode) []int {
	res := make([]int, 0)
	traversal(root, &res)
	return res
}

func traversal(root *TreeNode, elem *[]int) {
	if root == nil {
		return
	}
	*elem = append(*elem, root.Val)
	traversal(root.Left, elem)
	traversal(root.Right, elem)
}
