[LRU Cache](https://leetcode.com/problems/lru-cache/)

**方法一：双向链表+哈希表**

```go
type LRUCache struct {
	elem     *list.List
	hashMap  map[int]*list.Element
	capacity int
}

func Constructor(capacity int) LRUCache {
	return LRUCache{
		elem:     list.New(),
		hashMap:  make(map[int]*list.Element),
		capacity: capacity,
	}
}

func (this *LRUCache) exist(key int) bool {
	_, ok := this.hashMap[key]
	return ok
}

func (this *LRUCache) removeToFront(e *list.Element) {
	this.elem.MoveToFront(e)
}

func (this *LRUCache) Get(key int) int {
	// 存在：返回并移至队列头部
	// 不存在：直接返回-1
	if !this.exist(key) {
		return -1
	}
	v := this.hashMap[key]
	this.removeToFront(v)
	return v.Value.([]int)[1]
}

func (this *LRUCache) Put(key int, value int) {
	//如果关键字 key 已经存在，则变更其数据值 value，并移至队列头部
	if this.exist(key) {
		v := this.hashMap[key]
		v.Value = []int{key, value}
		this.removeToFront(v)
		return
	}
	//如果插入操作导致关键字数量超过 capacity ，则应该逐出最久未使用的关键字。
	if this.elem.Len() == this.capacity {
		v := this.elem.Back()
		key := this.elem.Remove(v).([]int)[0]
		delete(this.hashMap, key)
	}
	//如果不存在，则向缓存中插入该组 key-value 。
	this.elem.PushFront([]int{key, value})
	this.hashMap[key] = this.elem.Front()
}

/**
 * Your LRUCache object will be instantiated and called as such:
 * obj := Constructor(capacity);
 * param_1 := obj.Get(key);
 * obj.Put(key,value);
 */
```
