package leetcode

import (
	"gitee.com/ombak/leetcode/pkg/list"
)

// https://leetcode.com/problems/intersection-of-two-linked-lists/
func getIntersectionNode(headA, headB *list.ListNode) *list.ListNode {
	a1, b1 := headA, headB
	for a1 != b1 {
		if a1 == nil {
			a1 = headB
		} else {
			a1 = a1.Next
		}
		if b1 == nil {
			b1 = headA
		} else {
			b1 = b1.Next
		}
	}
	return a1
}
