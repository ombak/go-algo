package leetcode

import (
	"fmt"
	"testing"

	"gitee.com/ombak/leetcode/pkg/list"
)

func TestGetIntersectionNode(t *testing.T) {
	l1, l2 := list.NewRandomList(), list.NewRandomList()
	fmt.Println(getIntersectionNode(l1, l2))
}
