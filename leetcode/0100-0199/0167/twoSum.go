package leetcode

// https://leetcode.com/problems/two-sum-ii-input-array-is-sorted/
func twoSum(numbers []int, target int) []int {
	res := make([]int, 0)
	s, e := 0, len(numbers)-1
	for s < e {
		if numbers[s]+numbers[e] > target {
			e--
		} else if numbers[s]+numbers[e] < target {
			s++
		} else {
			break
		}
	}
	if s >= e {
		return res
	}
	res = append(res, s+1, e+1)
	return res
}
