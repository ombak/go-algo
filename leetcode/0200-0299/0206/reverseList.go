package leetcode

import (
	"gitee.com/ombak/leetcode/pkg/list"
)

// https://leetcode.com/problems/reverse-linked-list/
// solution by iteration
func reverseList1(head *list.ListNode) *list.ListNode {
	if head == nil || head.Next == nil {
		return head
	}
	pre, now := head, head.Next
	pre.Next = nil
	for now != nil {
		nowTmp := now.Next
		now.Next = pre
		pre = now
		now = nowTmp
	}
	return pre
}

// solution by recursive
func reverseList(head *list.ListNode) *list.ListNode {
	if head == nil || head.Next == nil {
		return head
	}
	last := reverseList(head.Next)
	head.Next.Next = head
	head.Next = nil
	return last
}
