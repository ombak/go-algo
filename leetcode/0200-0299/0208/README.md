[实现 Trie (前缀树)](https://leetcode.cn/problems/implement-trie-prefix-tree/)

**方法一：Trie 树**

才疏学浅，只会抄袭，参考 [labuladong](https://mp.weixin.qq.com/s/hGrTUmM1zusPZZ0nA9aaNw) 吧!

```go
type Trie struct {
	exist    bool
	children [26]*Trie
}

func Constructor() Trie {
	return Trie{}
}

func (this *Trie) Insert(word string) {
	var dfs func(node *Trie, start int) *Trie
	dfs = func(node *Trie, start int) *Trie {
		if node == nil {
			node = new(Trie)
		}
		if start == len(word) {
			node.exist = true
			return node
		}
		idx := rune(word[start]) - 'a'
		node.children[idx] = dfs(node.children[idx], start+1)
		return node
	}
	dfs(this, 0)
}

func (this *Trie) Search(word string) bool {
	node := this.getNode(word)
	return node != nil && node.exist
}

func (this *Trie) StartsWith(prefix string) bool {
	return this.getNode(prefix) != nil
}

func (this *Trie) getNode(word string) *Trie {
	p := this
	for _, c := range word {
		if p == nil {
			return nil
		}
		idx := c - 'a'
		p = p.children[idx]
	}
	return p
}

/**
 * Your Trie object will be instantiated and called as such:
 * obj := Constructor();
 * obj.Insert(word);
 * param_2 := obj.Search(word);
 * param_3 := obj.StartsWith(prefix);
 */
```
