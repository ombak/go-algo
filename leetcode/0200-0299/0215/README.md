[Kth Largest Element in an Array](https://leetcode.com/problems/kth-largest-element-in-an-array/)

**方法一：快速排序**

```go
// 洗牌算法，避免 partition 最坏情况的出现
func shuffle(nums []int) {
	n := len(nums)
	for i := 0; i < n; i++ {
		index := i + rand.Intn(n-i)
		nums[i], nums[index] = nums[index], nums[i]
	}
}

func findKthLargest(nums []int, k int) int {
	shuffle(nums)
	left, right := 0, len(nums)-1
	t, target := partition(nums, left, right), len(nums)-k
	for t != target {
		if t < target {
			left = t + 1
		} else {
			right = t - 1
		}
		t = partition(nums, left, right)
	}
	return nums[target]
}

func partition(nums []int, left, right int) int {
	l, r, target := left, right-1, nums[right]
	for l <= r {
		for l <= r && nums[l] <= target {
			l++
		}
		for l <= r && nums[r] >= target {
			r--
		}
		if l < r {
			nums[l], nums[r] = nums[r], nums[l]
		}
	}
	nums[l], nums[right] = target, nums[l]
	return l
}
```

**方法二：优先队列**

略
