[Count Complete Tree Nodes](https://leetcode.com/problems/count-complete-tree-nodes/)

**方法一：递归遍历**

时间复杂度仍旧是 O(n)，虽然可以通过测评，但不符合题意要求，因为未使用到完全二叉树的性质。

```go
/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
func countNodes(root *TreeNode) int {
	if root == nil {
		return 0
	}
	return countNodes(root.Left) + countNodes(root.Right) + 1
}
```

**方法二：递归+剪枝**

通过剪枝降低时间复杂度为 O(logN\*logN)。这是因为最后的两个递归只有一个会真的递归下去，另一个一定会触发 hl == hr 而立即返回，不会递归下去。而递归深度就是树的高度为 O(logN)，每次递归所花费的时间是计算树中节点的高度 O(logN)，所以总体的时间复杂度是 O(logN\*logN)。

```go
/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
func countNodes(root *TreeNode) int {
	if root == nil {
		return 0
	}
	l, r, hl, hr := root, root, 0, 0
	for ; l != nil; l = l.Left {
		hl++
	}
	for ; r != nil; r = r.Right {
		hr++
	}
	if hl == hr {
		return int(math.Pow(2, float64(hl))) - 1
	}
	return countNodes(root.Left) + countNodes(root.Right) + 1
}
```
