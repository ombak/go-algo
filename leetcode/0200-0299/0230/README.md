[Kth Smallest Element in a BST](https://leetcode.com/problems/kth-smallest-element-in-a-bst/)

**方法一：中序遍历**

```go
/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
func kthSmallest(root *TreeNode, k int) int {
	stack := make([]*TreeNode, 0)
	for {
		for root != nil {
			stack = append(stack, root)
			root = root.Left
		}
		root, stack = stack[len(stack)-1], stack[:len(stack)-1]
		k--
		if k == 0 {
			return root.Val
		}
		root = root.Right
	}
}
```

**方法二：记录子树的节点数**

```go
/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */

type MyBST struct {
	root    *TreeNode
	nodeNum map[*TreeNode]int
}

func (tree *MyBST) construct(root *TreeNode) int {
	if root == nil {
		return 0
	}
	tree.nodeNum[root] = tree.construct(root.Left) + tree.construct(root.Right) + 1
	return tree.nodeNum[root]
}

func (tree *MyBST) kthSmallest(k int) int {
	root := tree.root
	for {
		leftTreeNum := tree.nodeNum[root.Left]
		if leftTreeNum < k-1 {
			root = root.Right
			k = k - leftTreeNum - 1
		} else if leftTreeNum == k-1 {
			return root.Val
		} else {
			root = root.Left
		}
	}
}

func kthSmallest(root *TreeNode, k int) int {
	bst := &MyBST{root: root, nodeNum: make(map[*TreeNode]int)}
	bst.construct(root)
	return bst.kthSmallest(k)
}
```
