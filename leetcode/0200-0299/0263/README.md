[Ugly Number](https://leetcode.com/problems/ugly-number/)

「丑数」系列问题还包括：

- [264. Ugly Number II](https://leetcode.com/problems/ugly-number-ii/)
- [1201. Ugly Number III](https://leetcode.com/problems/ugly-number-iii/)
- [313. Super Ugly Number](https://leetcode.com/problems/super-ugly-number/)

**方法一：算术基本定理**

[算术基本定理](https://zh.wikipedia.org/wiki/%E7%AE%97%E6%9C%AF%E5%9F%BA%E6%9C%AC%E5%AE%9A%E7%90%86)，又称为正整数的唯一分解定理，即：每个大于 1 的自然数，要么本身就是质数，要么可以写为 2 个或以上的质数的积。根据算术基本定理的性质可知丑数也可以被任意多个质数的乘积分解，故现在只需要确定使用质数 2、3、5 是否可以拆分变量 n 即可。

```go
func isUgly(n int) bool {
	if n < 1 {
		return false
	}
	for _, v := range []int{2, 3, 5} {
		for n%v == 0 {
			n /= v
		}
	}
	return n == 1
}
```
