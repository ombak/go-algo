[Ugly Number II](https://leetcode.com/problems/ugly-number-ii/)

「丑数」系列问题还包括：

- [263. Ugly Number](https://leetcode.com/problems/ugly-number/)
- [1201. Ugly Number III](https://leetcode.com/problems/ugly-number-iii/)
- [313. Super Ugly Number](https://leetcode.com/problems/super-ugly-number/)

**方法一：模拟**

根据题目要求，模拟第 1 ～ n 个丑数。其中最重要的一个性质是任意一个丑数乘以 2，3，5 其仍旧是丑数。

```go
func nthUglyNumber(n int) int {
	ugly, p := make([]int, n+1), 1
	p2, p3, p5 := 1, 1, 1
	target2, target3, target5 := 1, 1, 1
	for p <= n {
		ugly[p], p = min(target2, target3, target5), p+1
		if ugly[p-1] == target2 {
			target2, p2 = 2*ugly[p2], p2+1
		}
		if ugly[p-1] == target3 {
			target3, p3 = 3*ugly[p3], p3+1
		}
		if ugly[p-1] == target5 {
			target5, p5 = 5*ugly[p5], p5+1
		}
	}
	return ugly[n]
}

func min(a, b, c int) int {
	if a < b {
		if a < c {
			return a
		}
		return c
	}
	if b < c {
		return b
	}
	return c
}
```
