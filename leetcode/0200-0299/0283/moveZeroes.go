package leetcode

// https://leetcode.com/problems/move-zeroes/
func moveZeroes(nums []int) {
	l, r := 0, 0
	for ; r < len(nums); r++ {
		if nums[r] != 0 {
			nums[l] = nums[r]
			l++
		}
	}
	for ; l < len(nums); l++ {
		nums[l] = 0
	}
}
