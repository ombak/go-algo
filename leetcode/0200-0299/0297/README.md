[Serialize and Deserialize Binary Tree](https://leetcode.com/problems/serialize-and-deserialize-binary-tree/)

**方法一：前序遍历**  

```go
type Codec struct {
	empty, sep string
}

func Constructor() Codec {
	return Codec{empty: "#", sep: ","}
}

// Serializes a tree to a single string.
func (this *Codec) serialize(root *TreeNode) string {
	if root == nil {
		return this.empty + this.sep
	}
	res := strconv.Itoa(root.Val) + this.sep
	res += this.serialize(root.Left)
	res += this.serialize(root.Right)
	return res
}

// Deserializes your encoded data to tree.
func (this *Codec) deserialize(data string) *TreeNode {
	nodes := strings.Split(strings.TrimSuffix(data, this.sep), this.sep)
	var dfs func() *TreeNode
	dfs = func() *TreeNode {
		if len(nodes) == 0 {
			return nil
		}
		node := nodes[0]
		nodes = nodes[1:]
		if node == this.empty {
			return nil
		}
		value, _ := strconv.Atoi(node)
		root := &TreeNode{Val: value}
		root.Left = dfs()
		root.Right = dfs()
		return root
	}
	return dfs()
}

/**
 * Your Codec object will be instantiated and called as such:
 * ser := Constructor();
 * deser := Constructor();
 * data := ser.serialize(root);
 * ans := deser.deserialize(data);
 */
```

**方法二：后序遍历**

```go
type Codec struct {
	empty, sep string
}

func Constructor() Codec {
	return Codec{empty: "#", sep: ","}
}

// Serializes a tree to a single string.
func (this *Codec) serialize(root *TreeNode) string {
	if root == nil {
		return this.empty + this.sep
	}
	res := ""
	res += this.serialize(root.Left)
	res += this.serialize(root.Right)
	res += strconv.Itoa(root.Val) + this.sep
	return res
}

// Deserializes your encoded data to tree.
func (this *Codec) deserialize(data string) *TreeNode {
	nodes := strings.Split(strings.TrimSuffix(data, this.sep), this.sep)
	var dfs func() *TreeNode
	dfs = func() *TreeNode {
		if len(nodes) == 0 {
			return nil
		}
		node := nodes[len(nodes)-1]
		nodes = nodes[:len(nodes)-1]
		if node == this.empty {
			return nil
		}
		value, _ := strconv.Atoi(node)
		root := &TreeNode{Val: value}
		root.Right = dfs()
		root.Left = dfs()
		return root
	}
	return dfs()
}

/**
 * Your Codec object will be instantiated and called as such:
 * ser := Constructor();
 * deser := Constructor();
 * data := ser.serialize(root);
 * ans := deser.deserialize(data);
 */
```

**方法三：层序遍历**

```go
// Definition for a binary tree node.
// type TreeNode struct {
// 	Val   int
// 	Left  *TreeNode
// 	Right *TreeNode
// }

type Codec struct {
	empty, sep string
}

func Constructor() Codec {
	return Codec{empty: "#", sep: ","}
}

// Serializes a tree to a single string.
func (this *Codec) serialize(root *TreeNode) string {
	if root == nil {
		return ""
	}
	queue, res := make([]*TreeNode, 1), ""
	queue[0] = root
	for len(queue) > 0 {
		front := queue[0]
		queue = queue[1:]
		if front == nil {
			res += this.empty + this.sep
			continue
		}
		res += strconv.Itoa(front.Val) + this.sep
		queue = append(queue, front.Left, front.Right)
	}
	return res
}

// Deserializes your encoded data to tree.
func (this *Codec) deserialize(data string) *TreeNode {
	if data == "" {
		return nil
	}
	nodes := strings.Split(strings.TrimSuffix(data, this.sep), this.sep)
	value, _ := strconv.Atoi(nodes[0])
	queue, root := make([]*TreeNode, 1), &TreeNode{Val: value}
	queue[0] = root

	for i := 1; i < len(nodes); {
		front, node := queue[0], ""
		queue = queue[1:]
		// 左子树
		node, i = nodes[i], i+1
		if node != this.empty {
			value, _ = strconv.Atoi(node)
			front.Left = &TreeNode{Val: value}
			queue = append(queue, front.Left)
		}

		// 右子树
		node, i = nodes[i], i+1
		if node != this.empty {
			value, _ = strconv.Atoi(node)
			front.Right = &TreeNode{Val: value}
			queue = append(queue, front.Right)
		}
	}
	return root
}

/**
 * Your Codec object will be instantiated and called as such:
 * ser := Constructor();
 * deser := Constructor();
 * data := ser.serialize(root);
 * ans := deser.deserialize(data);
 */
```
