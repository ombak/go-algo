package leetcode

// https://leetcode.com/problems/range-sum-query-immutable/
type NumArray struct {
	preSumNums []int
}

// calculate prefix sum.
func Constructor(nums []int) NumArray {
	pres := make([]int, len(nums)+1)
	for i := 1; i < len(pres); i++ {
		pres[i] = pres[i-1] + nums[i-1]
	}
	return NumArray{preSumNums: pres}
}

// return the sum of the elements of nums between indices left and right inclusive.
func (t *NumArray) SumRange(left int, right int) int {
	return t.preSumNums[right+1] - t.preSumNums[left]
}

/**
 * Your NumArray object will be instantiated and called as such:
 * obj := Constructor(nums);
 * param_1 := obj.SumRange(left,right);
 */
