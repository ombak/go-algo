package leetcode

// https://leetcode.com/problems/range-sum-query-2d-immutable/
type NumMatrix struct {
	preSum [][]int
}

func Constructor(matrix [][]int) NumMatrix {
	if len(matrix) < 1 {
		return NumMatrix{}
	}
	// init
	m, n := len(matrix), len(matrix[0])
	preSum := make([][]int, m+1)
	for i := range preSum {
		preSum[i] = make([]int, n+1)
	}
	// calc
	for i := 1; i <= m; i++ {
		for j := 1; j <= n; j++ {
			preSum[i][j] = preSum[i-1][j] + preSum[i][j-1] + matrix[i-1][j-1] - preSum[i-1][j-1]
		}
	}
	return NumMatrix{preSum: preSum}
}

func (t *NumMatrix) SumRegion(row1 int, col1 int, row2 int, col2 int) int {
	return t.preSum[row2+1][col2+1] - t.preSum[row1][col2+1] - t.preSum[row2+1][col1] + t.preSum[row1][col1]
}

/**
 * Your NumMatrix object will be instantiated and called as such:
 * obj := Constructor(matrix);
 * param_1 := obj.SumRegion(row1,col1,row2,col2);
 */
