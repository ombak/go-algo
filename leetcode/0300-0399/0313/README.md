[313. Super Ugly Number](https://leetcode.com/problems/super-ugly-number/)

**方法一：优先队列**

根据题目要求，模拟第 1 ～ n 个丑数，显然任意一个丑数 `ugly[i]` 乘以 `primes` 数组中的质数 `primes[i]` 仍旧是丑数。已知 `ugly[1]=1`，`ugly[2] = min(ugly[1]*primes[i])`，则 `ugly[3]` 应该等于集合 `ugly[2]*primes[i], ugly[1]*primes[i]` 中排除 `ugly[2]` 之后的最小值，依次类推可以得到 `ugly[n]` 的值。此外丑数数列是单调递增序列，可以维护一个优先队列，每次操作时先移除最小的元素放入丑数序列，然后乘以对应的 prime 之后继续放入队列即可。

**_Go_**

```go
type Ugly struct{ value, prime, index int }
type Queue []Ugly

func (u Queue) Len() int            { return len(u) }
func (u Queue) Swap(i, j int)       { u[i], u[j] = u[j], u[i] }
func (u Queue) Less(i, j int) bool  { return u[i].value < u[j].value }
func (u *Queue) Push(v interface{}) { *u = append(*u, v.(Ugly)) }
func (u *Queue) Pop() interface{} {
	old, x := *u, (*u)[len(*u)-1]
	*u = old[:len(old)-1]
	return x
}

func nthSuperUglyNumber(n int, primes []int) int {
	ugly, pq, p := make([]int, n+1), &Queue{}, 2
	ugly[1] = 1
	heap.Init(pq)
	for _, v := range primes {
		heap.Push(pq, Ugly{value: v, prime: v, index: 2})
	}
	for p <= n {
		top := heap.Pop(pq).(Ugly)
		if ugly[p-1] != top.value {
			ugly[p], p = top.value, p+1
		}
		top.value, top.index = ugly[top.index]*top.prime, top.index+1
		heap.Push(pq, top)
	}
	return ugly[n]
}
```

**_Python3_**

```py
from queue import PriorityQueue


class Solution:
    def nthSuperUglyNumber(self, n: int, primes: List[int]) -> int:
        ugly, pq, p = [0]*(n+1), PriorityQueue(), 2
        ugly[1] = 1
        for prime in primes:
            pq.put([prime, prime, 2])

        while p <= n:
            top = pq.get()
            if top[0] != ugly[p-1]:
                ugly[p], p = top[0], p+1
            top[0], top[2] = ugly[top[2]]*top[1], top[2]+1
            pq.put(top)
        return ugly[n]
```
