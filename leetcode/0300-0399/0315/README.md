[Count of Smaller Numbers After Self](https://leetcode.com/problems/count-of-smaller-numbers-after-self/)

**方法一：归并排序**

计算过程和排序过程分离（简单易懂）：

```go
type Pair struct {
	val   int
	index int
}

var (
	tmp   []Pair
	count []int
)

func countSmaller(nums []int) []int {
	tmp, count = make([]Pair, len(nums)), make([]int, len(nums))
	array := make([]Pair, len(nums))
	for i, v := range nums {
		array[i] = Pair{val: v, index: i}
	}
	sorted(array, 0, len(array)-1)
	return count
}

func sorted(arr []Pair, low, high int) {
	if low >= high {
		return
	}
	mid := low + (high-low)/2
	sorted(arr, low, mid)
	sorted(arr, mid+1, high)
	merge(arr, low, mid, high)
}

func merge(nums []Pair, low, mid, high int) {
	for i, j := low, mid+1; i <= mid; i++ {
		for j <= high && nums[i].val > nums[j].val {
			j++
		}
		count[nums[i].index] += j - mid - 1
	}

	i, j, idx := low, mid+1, low
	for i <= mid && j <= high {
		if nums[i].val <= nums[j].val {
			tmp[idx], idx, i = nums[i], idx+1, i+1
		} else {
			tmp[idx], idx, j = nums[j], idx+1, j+1
		}
	}
	for i <= mid {
		tmp[idx], idx, i = nums[i], idx+1, i+1
	}
	for j <= high {
		tmp[idx], idx, j = nums[j], idx+1, j+1
	}

	for i = low; i <= high; i++ {
		nums[i] = tmp[i]
	}
}
```

计算过程融入到排序过程中（可以窥见上面👆算法到本质）：

```go
type Pair struct {
	val   int
	index int
}

var (
	tmp   []Pair
	count []int
)

func countSmaller(nums []int) []int {
	tmp, count = make([]Pair, len(nums)), make([]int, len(nums))
	array := make([]Pair, len(nums))
	for i, v := range nums {
		array[i] = Pair{val: v, index: i}
	}
	sorted(array, 0, len(array)-1)
	return count
}

func sorted(arr []Pair, low, high int) {
	if low >= high {
		return
	}
	mid := low + (high-low)/2
	sorted(arr, low, mid)
	sorted(arr, mid+1, high)
	merge(arr, low, mid, high)
}

func merge(arr []Pair, low, mid, high int) {
	left, right := low, mid+1
	idx := low
	for left <= mid && right <= high {
		if arr[left].val <= arr[right].val {
			count[arr[left].index] += right - mid - 1
			tmp[idx], left = arr[left], left+1
		} else {
			tmp[idx], right = arr[right], right+1
		}
		idx++
	}
	for left <= mid {
		count[arr[left].index] += right - mid - 1
		tmp[idx] = arr[left]
		idx, left = idx+1, left+1
	}
	for right <= high {
		tmp[idx] = arr[right]
		idx, right = idx+1, right+1
	}
	// 排序
	for i := low; i <= high; i++ {
		arr[i] = tmp[i]
	}
}
```

**方法二：树状数组**  

[仰望大佬🧍‍♂️，小菜鸡不配](https://leetcode.cn/problems/count-of-smaller-numbers-after-self/solution/shu-zhuang-shu-zu-by-liweiwei1419/)

**方法三：线段树**
