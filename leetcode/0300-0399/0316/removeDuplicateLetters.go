package leetcode

// https://leetcode.com/problems/remove-duplicate-letters/
func removeDuplicateLetters(s string) string {
	count, inStack, stack := make([]int, 128), make([]bool, 128), make([]rune, 0)
	for _, c := range s {
		count[c] += 1
	}

	for _, c := range s {
		count[c] -= 1
		if inStack[c] {
			continue
		}
		for len(stack) > 0 && stack[len(stack)-1] > c && count[stack[len(stack)-1]] > 0 {
			peek := stack[len(stack)-1]
			stack = stack[0 : len(stack)-1]
			inStack[peek] = false
		}
		stack = append(stack, c)
		inStack[c] = true
	}
	return string(stack)
}

// func main() {
// 	s := "bcabc"
// 	fmt.Println(removeDuplicateLetters(s))
// 	s = "bcac"
// 	fmt.Println(removeDuplicateLetters(s))
// 	s = ""
// 	fmt.Println(removeDuplicateLetters(s))
// }
