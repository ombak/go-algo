[Count of Range Sum](https://leetcode.com/problems/count-of-range-sum/)

**方法一：归并排序+前缀和**

```go
func countRangeSum(nums []int, lower int, upper int) int {
	preSum, tmp := make([]int, len(nums)+1), make([]int, len(nums)+1)
	for i := 0; i < len(nums); i++ {
		preSum[i+1] = preSum[i] + nums[i]
	}
	var sorted func(arr []int, low, high int) int
	sorted = func(arr []int, low, high int) int {
		if low >= high {
			return 0
		}
		mid := low + (high-low)/2
		res := sorted(arr, low, mid) + sorted(arr, mid+1, high)
		// merge
		start, end := mid+1, mid+1
		for i := low; i <= mid; i++ {
			// 第一个区间和 >= lower 的数字
			for start <= high && arr[start]-arr[i] < lower {
				start++
			}
			// 第一个区间和 > upper 的数字
			for end <= high && arr[end]-arr[i] <= upper {
				end++
			}
			res += end - start
		}
		merge(arr, tmp, low, mid, high)
		////////
		return res
	}
	return sorted(preSum, 0, len(preSum)-1)
}

func merge(nums, tmp []int, low, mid, high int) {
	i, j, index := low, mid+1, low
	for i <= mid && j <= high {
		if nums[i] <= nums[j] {
			tmp[index], index, i = nums[i], index+1, i+1
		} else {
			tmp[index], index, j = nums[j], index+1, j+1
		}
	}
	for i <= mid {
		tmp[index], index, i = nums[i], index+1, i+1
	}
	for j <= high {
		tmp[index], index, j = nums[j], index+1, j+1
	}

	for i = low; i <= high; i++ {
		nums[i] = tmp[i]
	}
}
```
