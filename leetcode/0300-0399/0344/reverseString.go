package leetcode

// https://leetcode.com/problems/reverse-string/
func reverseString(s []byte) {
	st, ed := 0, len(s)-1
	for st < ed {
		s[st], s[ed] = s[ed], s[st]
		st++
		ed--
	}
}
