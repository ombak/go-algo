package leetcode

import (
	"fmt"
	"testing"
)

func Test_findAnagrams(t *testing.T) {
	s1 := findAnagrams("cbaebabacd", "abc")
	s2 := findAnagrams("     ", " ")
	fmt.Println(s1, s2)
}
