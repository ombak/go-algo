[Delete Node in a BST](https://leetcode.com/problems/delete-node-in-a-bst/)

**方法一：递归**

在对二叉树进行遍历的过程中，当发现 root 节点为要查找的节点时考虑一下三种情况进行分类处理：

- root 节点为叶子结点，直接返回 nil

- root 节点的左子树/右子树为空，直接返回右子树/左子树

- root 节点左右子树均不为空，有两种解决方法  
  （1）直接将左子树作为右子树上最小的节点的左子树，然后右子树作为新的根节点即可  
  （2）从右子树中删除最小的节点，并将其作为新的根节点，其左右子树为原来根节点的左右子树

直接将左子树作为右子树上最小的节点的左子树：

```go
/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
func deleteNode(root *TreeNode, key int) *TreeNode {
	if root == nil {
		return nil
	}
	if root.Val == key {
		if root.Left == nil {
			return root.Right
		} else if root.Right == nil {
			return root.Left
		}
		// all not nil
		minRight := root.Right
		for minRight.Left != nil {
			minRight = minRight.Left
		}
		minRight.Left = root.Left
		root = root.Right
	} else if root.Val > key {
		root.Left = deleteNode(root.Left, key)
	} else {
		root.Right = deleteNode(root.Right, key)
	}
	return root
}
```

右子树最小的节点作为新的根节点：

```go
/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
func deleteNode(root *TreeNode, key int) *TreeNode {
	if root == nil {
		return nil
	}
	if root.Val == key {
		if root.Left == nil {
			return root.Right
		} else if root.Right == nil {
			return root.Left
		}
		// root.Left && root.Right != nil
		minRight := root.Right
		for minRight.Left != nil {
			minRight = minRight.Left
		}
		root.Right = deleteNode(root.Right, minRight.Val)
		minRight.Left, minRight.Right = root.Left, root.Right
		root = minRight
	} else if root.Val > key {
		root.Left = deleteNode(root.Left, key)
	} else {
		root.Right = deleteNode(root.Right, key)
	}
	return root
}
```
