[LFU Cache](https://leetcode.com/problems/lfu-cache/)

**方法一：哈希表**

```go
type LFUCache struct {
	keyToValue map[int]int
	keyToFreq  map[int]int
	freqToKeys map[int]*list.List
	keyToKeys  map[int]*list.Element
	minFreq    int
	capacity   int
}

func Constructor(capacity int) LFUCache {
	res := LFUCache{
		keyToValue: make(map[int]int),
		keyToFreq:  make(map[int]int),
		freqToKeys: make(map[int]*list.List),
		keyToKeys:  make(map[int]*list.Element),
		capacity:   capacity,
	}
	res.freqToKeys[0], res.freqToKeys[1] = list.New(), list.New()
	return res
}

func (this *LFUCache) freqUpgrade(freq, key int) {
	pointer := this.keyToKeys[key]
	if _, ok := this.freqToKeys[freq+1]; !ok {
		this.freqToKeys[freq+1] = list.New()
	}
	this.freqToKeys[freq].Remove(pointer)
	this.freqToKeys[freq+1].PushFront(pointer.Value.(int))
	this.keyToKeys[key] = this.freqToKeys[freq+1].Front()
	this.keyToFreq[key] += 1

	if this.freqToKeys[this.minFreq].Len() == 0 {
		this.minFreq = freq + 1
	}
}

func (this *LFUCache) removeLFU() {
	v := this.freqToKeys[this.minFreq].Back()
	key := v.Value.(int)
	this.freqToKeys[this.minFreq].Remove(v)
	delete(this.keyToKeys, key)
	delete(this.keyToFreq, key)
	delete(this.keyToValue, key)
}

func (this *LFUCache) Get(key int) int {
	if _, ok := this.keyToValue[key]; !ok {
		return -1
	}
	this.freqUpgrade(this.keyToFreq[key], key)
	return this.keyToValue[key]
}

func (this *LFUCache) Put(key int, value int) {
	if this.capacity <= 0 {
		return
	}
	// 如果键 key 已存在，则变更其值
	if _, ok := this.keyToValue[key]; ok {
		this.keyToValue[key] = value
		this.freqUpgrade(this.keyToFreq[key], key)
		return
	}
	// 当缓存达到其容量 capacity 时，则应该在插入新项之前，移除最不经常使用的项。
	if this.capacity == len(this.keyToValue) {
		this.removeLFU()
	}
	this.minFreq = 1
	this.keyToValue[key] = value
	this.keyToFreq[key] = 1
	this.freqToKeys[1].PushFront(key)
	this.keyToKeys[key] = this.freqToKeys[1].Front()
}

/**
 * Your LFUCache object will be instantiated and called as such:
 * obj := Constructor(capacity);
 * param_1 := obj.Get(key);
 * obj.Put(key,value);
 */
```
