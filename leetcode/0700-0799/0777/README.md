[Swap Adjacent in LR String](https://leetcode.com/problems/swap-adjacent-in-lr-string/)

**方法一：双指针**

**_Go_**

```go
func canTransform(start string, end string) bool {
	st1, st2, n := 0, 0, len(start)
	for st1 < n && st2 < n {
		for st1 < n && string(start[st1]) == "X" {
			st1++
		}
		for st2 < n && string(end[st2]) == "X" {
			st2++
		}
		if st1 < n && st2 < n {
			if start[st1] != end[st2] {
				return false
			}
			if string(end[st2]) == "L" && st2 > st1 {
				return false
			}
			if string(end[st2]) == "R" && st2 < st1 {
				return false
			}
			st1, st2 = st1+1, st2+1
		}
	}
	for ; st1 < n; st1++ {
		if string(start[st1]) != "X" {
			return false
		}
	}
	for ; st2 < n; st2++ {
		if string(end[st2]) != "X" {
			return false
		}
	}
	return true
}
```
