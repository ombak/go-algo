[链表组件](https://leetcode.cn/problems/linked-list-components/)

**方法一：哈希**

遍历链表，对于每一个值在子集中寻找是否存在，不存在的话直接跳过，存在的话，分下面两张情况进行讨论：

- 当前元素的前一个元素不在子集中，则组件数+1
- 当前元素的前一个元素在子集中，则直接跳过

```go
/**
 * Definition for singly-linked list.
 * type ListNode struct {
 *     Val int
 *     Next *ListNode
 * }
 */
func numComponents(head *ListNode, nums []int) int {
	numsMap := make([]bool, 10001)
	for _, v := range nums {
		numsMap[v] = true
	}
	components, pre := 0, false

	for ; head != nil; head = head.Next {
		if !numsMap[head.Val] {
			pre = false
		} else {
			if !pre {
				components++
			}
			pre = true
		}
	}
	return components
}
```
