[Score of Parentheses](https://leetcode.com/problems/score-of-parentheses/)

同类型的题目还有：

- 678.[有效的括号字符串](https://leetcode.cn/problems/valid-parenthesis-string/)

- 1096.[花括号展开 II](https://leetcode.cn/problems/brace-expansion-ii/)

- 1021.[删除最外层的括号](https://leetcode.cn/problems/remove-outermost-parentheses/)

- 1249.[移除无效的括号](https://leetcode.cn/problems/minimum-remove-to-make-valid-parentheses/)

- 1541.[平衡括号字符串的最少插入次数](https://leetcode.cn/problems/minimum-insertions-to-balance-a-parentheses-string/)

- 2116.[判断一个括号字符串是否有效](https://leetcode.cn/problems/check-if-a-parentheses-string-can-be-valid/)

**方法一：计数**

```go
func scoreOfParentheses(s string) int {
	ans, depth := 0, 0
	for i, c := range s {
		if c == '(' {
			depth++
		} else {
			depth--
			if s[i-1] == '(' {
				ans += 1 << depth
			}
		}
	}
	return ans
}
```

**方法二：栈**

```go
func scoreOfParentheses(s string) int {
	stack := []int{0}
	for _, c := range s {
		if c == '(' {
			stack = append(stack, 0)
		} else {
			cur, pre := stack[len(stack)-1], stack[len(stack)-2]
			stack = stack[:len(stack)-2]
			if cur*2 > 1 {
				stack = append(stack, pre+cur*2)
			} else {
				stack = append(stack, pre+1)
			}
		}
	}
	return stack[len(stack)-1]
}
```
