[Advantage Shuffle](https://leetcode.com/problems/advantage-shuffle/)

**方法一：排序**

田忌赛马类型的题目，使用**贪心+排序**的方式解决即可。

```go
func advantageCount(nums1 []int, nums2 []int) []int {
	ans, n, array2 := make([]int, len(nums1)), len(nums1), make([][]int, 0)
	sort.Ints(nums1)
	for i, v := range nums2 {
		array2 = append(array2, []int{i, v})
	}
	sort.Slice(array2, func(i, j int) bool { return array2[i][1] < array2[j][1] })
	for i, j, k := 0, n-1, n-1; k >= 0; k-- {
		if nums1[j] > array2[k][1] {
			ans[array2[k][0]], j = nums1[j], j-1
		} else {
			ans[array2[k][0]], i = nums1[i], i+1
		}
	}
	return ans
}
```
