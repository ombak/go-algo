package leetcode

// https://leetcode.com/problems/koko-eating-bananas/
func minEatingSpeed(piles []int, h int) int {
	left, right := 1, 0
	for _, v := range piles {
		if right < v {
			right = v
		}
	}
	for mid := left + (right-left)/2; left <= right; mid = left + (right-left)/2 {
		if check(piles, h, mid) {
			right = mid - 1
		} else {
			left = mid + 1
		}
	}
	return left
}

func check(piles []int, h, k int) bool {
	prek := 0
	for _, p := range piles {
		prek += (p + k - 1) / k
	}
	return prek <= h
}
