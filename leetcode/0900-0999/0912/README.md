[Sort an Array](https://leetcode.com/problems/sort-an-array/)

**方法一：归并排序**

```go
var tmp []int

func sortArray(nums []int) []int {
	tmp = make([]int, len(nums))
	var sorted func(arr []int, l, r int)
	sorted = func(arr []int, l, r int) {
		if l == r {
			return
		}
		mid := l + (r-l)/2
		sorted(arr, l, mid)
		sorted(arr, mid+1, r)
		merge(nums, l, mid, r)
	}
	sorted(nums, 0, len(nums)-1)
	return nums
}

func merge(arr []int, l, mid, r int) {
	x, y, idx := l, mid+1, l
	for x <= mid && y <= r {
		if arr[x] <= arr[y] {
			tmp[idx], x = arr[x], x+1
		} else {
			tmp[idx], y = arr[y], y+1
		}
		idx++
	}
	for x <= mid {
		tmp[idx] = arr[x]
		x, idx = x+1, idx+1
	}
	for y <= r {
		tmp[idx] = arr[y]
		y, idx = y+1, idx+1
	}
	for i := l; i <= r; i++ {
		arr[i] = tmp[i]
	}
}
```

**方法二：快速排序**

```go
func sortArray(nums []int) []int {
	quicksort(nums, 0, len(nums)-1)
	return nums
}

func quicksort(nums []int, left, right int) {
	if left >= right {
		return
	}
	mid := partition(nums, left, right)
	quicksort(nums, left, mid-1)
	quicksort(nums, mid+1, right)
}

func partition(nums []int, left, right int) int {
	com, l, r := nums[right], left, right-1
	for l <= r {
		for l <= r && nums[l] <= com {
			l++
		}
		for l <= r && nums[r] >= com {
			r--
		}
		if l < r {
			nums[l], nums[r] = nums[r], nums[l]
		}
	}
	nums[l], nums[right] = nums[right], nums[l]
	return l
}
```
