[不同的子序列 II](https://leetcode.cn/problems/distinct-subsequences-ii/)

**方法一：动态规划**

正常递推，递推公式为：

$$
dp[i] = dp[i-1] + newCount + repeatCount
$$

其中，$newCount = dp[i-1]$， $repeatCount = $`s[i] 上次出现时的新增子序列的数目`，最终则字符出的非空不同子序列数目为$dp[n-1]-1$，其中 `n` 为字符串的长度。

时间复杂度 $O(n)$，空间复杂度 $O(1)$，其中 `n` 为字符串的长度。

```go
func distinctSubseqII(s string) int {
	mod := int(1e9 + 7)
	repeatCount, curCount := make([]int, 128), 1
	for _, v := range s {
		newCount := curCount
		curCount = ((curCount+newCount)%mod - repeatCount[v] + mod) % mod
		repeatCount[v] = newCount
	}
    // 需要减去空串
	return curCount - 1
}
```
