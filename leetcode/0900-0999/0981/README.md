[Time Based Key-Value Store](https://leetcode.com/problems/time-based-key-value-store/)

**方法一：双哈希表**

```go
type TimeMap struct {
	ttov map[int]string
	ktot map[string][]int
}

func Constructor() TimeMap {
	return TimeMap{
		ttov: make(map[int]string),
		ktot: make(map[string][]int),
	}
}

func (this *TimeMap) Set(key string, value string, timestamp int) {
	this.ktot[key] = append(this.ktot[key], timestamp)
	this.ttov[timestamp] = value
}

func (this *TimeMap) Get(key string, timestamp int) string {
	times := this.ktot[key]
	i := sort.Search(len(times), func(i int) bool { return times[i] > timestamp })
	if i > 0 {
		return this.ttov[times[i-1]]
	}
	return ""
}

/**
 * Your TimeMap object will be instantiated and called as such:
 * obj := Constructor();
 * obj.Set(key,value,timestamp);
 * param_2 := obj.Get(key,timestamp);
 */
```

**方法二：哈希表+二分搜索**

```go
type pair struct {
	timestamp int
	value     string
}

type TimeMap struct {
	ktotv map[string][]pair
}

func Constructor() TimeMap {
	return TimeMap{ktotv: make(map[string][]pair)}
}

func (this *TimeMap) Set(key string, value string, timestamp int) {
	this.ktotv[key] = append(this.ktotv[key], pair{timestamp: timestamp, value: value})
}

func (this *TimeMap) Get(key string, timestamp int) string {
	tvs := this.ktotv[key]
	i := this.leftBound(tvs, timestamp)
	if i > 0 {
		return tvs[i-1].value
	}
	return ""
}

func (this *TimeMap) leftBound(data []pair, t int) int {
	left, right := 0, len(data)-1
	for left <= right {
		mid := left + (right-left)/2
		if data[mid].timestamp > t {
			right = mid - 1
		} else {
			left = mid + 1
		}
	}
	return left
}

/**
 * Your TimeMap object will be instantiated and called as such:
 * obj := Constructor();
 * obj.Set(key,value,timestamp);
 * param_2 := obj.Get(key,timestamp);
 */
```
