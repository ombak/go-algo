package leetcode

// https://leetcode.com/problems/corporate-flight-bookings/
func corpFlightBookings(bookings [][]int, n int) []int {
	diff := make([]int, n)
	for _, booking := range bookings {
		i, j, val := booking[0]-1, booking[1]-1, booking[2]
		diff[i] += val
		if j+1 < n {
			diff[j+1] -= val
		}
	}
	// result
	res := make([]int, n)
	res[0] = diff[0]
	for i := 1; i < n; i++ {
		res[i] = res[i-1] + diff[i]
	}
	return res
}
