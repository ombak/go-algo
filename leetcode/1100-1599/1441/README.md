[用栈操作构建数组](https://leetcode.cn/problems/build-an-array-with-stack-operations/)

**方法一：模拟**

模拟正常的栈操作即可，对于 1 ～ n 的每个元素先 Push，若该元素不在 target 数组中，直接 Pop 即可。

注意，当 target 数组已经构建好了则直接返回。

时间复杂度 $O(n)$，空间复杂度 $O(1)$。

```go
func buildArray(target []int, n int) []string {
	ans, p := make([]string, 0), 0
	for i := 1; i <= n && p < len(target); i++ {
		ans = append(ans, "Push")
		if i == target[p] {
			p++
		} else {
			ans = append(ans, "Pop")
		}
	}
	return ans
}
```
