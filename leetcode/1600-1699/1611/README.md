[Minimum One Bit Operations to Make Integers Zero](https://leetcode.com/problems/minimum-one-bit-operations-to-make-integers-zero/)

**方法一：动态规划**

详见题解[找规律递归](https://leetcode.cn/problems/minimum-one-bit-operations-to-make-integers-zero/solution/zhao-gui-lu-di-gui-by-a-zhu-8o-zndz/)，通过找规律可以发现动态规划转移方程如下：

dp[n] = dp[2<sup>k</sup>] - dp[n-2<sup>k</sup>]

其中 dp[2<sup>k</sup>] = 2<sup>k+1</sup>-1。k 表示小于等于 n 的最大的 2 的整数次幂的位数，即 2<sup>k</sup> 是小于等于 n 的最大的 2 的整数次幂。

**_Go_**

```go
func minimumOneBitOperations(n int) int {
	if n <= 1 {
		return n
	}
	base := 0
	for i := 0; i < 64; i++ {
		if (n >> i) == 1 {
			base = 1 << i
			break
		}
	}
	return (base << 1) - 1 - minimumOneBitOperations(n-base)
}
```

**_Python3_**

```python
class Solution:
    def minimumOneBitOperations(self, n: int) -> int:
        if n <= 1:
            return n
        for i in range(64):
            if (n >> i) == 1:
                base = 1 << i
                break
        return 2*base-1 - self.minimumOneBitOperations(n-base)
```
