[Defuse the Bomb](https://leetcode.com/problems/defuse-the-bomb/)

**方法一：前缀和数组**

```go
func decrypt(code []int, k int) []int {
	n := len(code)
	preSum := make([]int, 2*n+1)
	for i := 0; i < len(preSum)-1; i++ {
		preSum[i+1] = preSum[i] + code[i%n]
	}

	for i := range code {
		if k == 0 {
			code[i] = 0
		} else if k > 0 {
			code[i] = preSum[i+k+1] - preSum[i+1]
		} else {
            // 注意是前 k 个，不要写为 i+n+1
			code[i] = preSum[i+n] - preSum[i+k+n]
		}
	}
	return code
}
```

**方法二：暴力解法**

> 超出时间限制

```go
func decrypt(code []int, k int) []int {
	ans, n := make([]int, len(code)), len(code)
	for i := 0; i < n; i++ {
		if k > 0 {
			for j := i + 1; j <= j+k; j++ {
				ans[i] += code[j%n]
			}
		} else if k < 0 {
			for j := i + k; j < i; j++ {
				ans[i] += code[(j+n)%n]
			}
		}
	}
	return ans
}
```
