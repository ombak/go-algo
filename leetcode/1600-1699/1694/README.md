[Reformat Phone Number](https://leetcode.com/problems/reformat-phone-number/)

**方法一：根据题意模拟**

先按照题意，去除字符串中的所有空格和破折号。

记当前字符串长度为 n ，然后从前往后遍历字符串，每 3 个字符为一组，将其加入结果字符串中，共取 n/3 组。

若最后剩余 1 个字符，则将前面的最后一组的最后一个字符与该字符组成一个新的两个字符的组，加入结果字符串中；若最后剩余 2 个字符，直接将这连个字符组成一个新的组，加入结果字符串中。

最后将所有组之间加上破折号，返回结果字符串即可。时间复杂度 O(n)。

**_Go_**

```go
func reformatNumber(number string) string {
	res := ""
	for i := 0; i < len(number); i++ {
		if string(number[i]) != " " && string(number[i]) != "-" {
			res += string(number[i])
		}
	}
	left, right, n, r := 0, 3, len(res), ""
	for right < n {
		if n-right > 4 {
			r += res[left:right] + "-"
		} else {
			if n-right == 4 {
				r += res[left:right] + "-" + res[right:right+2] + "-" + res[right+2:]
			} else if n-right == 1 {
				r += res[left:left+2] + "-" + res[left+2:]
			} else {
				r += res[left:right] + "-" + res[right:]
			}
			return r
		}
		left, right = right, right+3
	}
	return res
}
```
