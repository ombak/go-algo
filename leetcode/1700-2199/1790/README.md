[仅执行一次字符串交换能否使两个字符串相等](https://leetcode.cn/problems/check-if-one-string-swap-can-make-strings-equal/)

**方法一：扫描**

找到前两对索引位置相同，但字符不同的索引之后，交换这两个字符

```go
func areAlmostEqual(v1 string, v2 string) bool {
	i1, i2 := 0, 0
	for ; i1 < len(v1); i1, i2 = i1+1, i2+1 {
		if v1[i1] != v2[i2] {
			break
		}
	}
	j1, j2 := i1+1, i2+1
	for ; j1 < len(v1); j1, j2 = j1+1, j2+1 {
		if v1[j1] != v2[j2] {
			break
		}
	}
	if j1 < len(v1) {
		v1 = v1[:i1] + v1[j1:j1+1] + v1[i1+1:j1] + v1[i1:i1+1] + v1[j1+1:]
	}
	return v1 == v2
}
```

**方法二：简单计数**

```go

```
