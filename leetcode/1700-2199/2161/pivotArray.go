package leetcode

// https://leetcode.com/problems/partition-array-according-to-given-pivot/
func pivotArray(nums []int, pivot int) []int {
	ans := make([]int, 0)
	equalNum := 0
	for i := 0; i < len(nums); i++ {
		if nums[i] < pivot {
			ans = append(ans, nums[i])
		} else if nums[i] == pivot {
			equalNum++
		}
	}
	for i := 0; i < equalNum; i++ {
		ans = append(ans, pivot)
	}
	for i := 0; i < len(nums); i++ {
		if nums[i] > pivot {
			ans = append(ans, nums[i])
		}
	}
	return ans
}
