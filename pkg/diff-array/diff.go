package diff

// diff array algorithm, detail:
// https://labuladong.gitee.io/algo/2/20/25/
type Difference struct {
	diff []int
}

// Constructor a new Difference Object.
func Constructor(array []int) Difference {
	if len(array) < 1 {
		panic("array empty")
	}
	diff := make([]int, len(array))
	diff[0] = array[0]
	for i := 1; i < len(array); i++ {
		diff[i] = array[i] - array[i-1]
	}
	return Difference{diff: diff}
}

// Increment diff array add certainly value 'val' from indices i to j.
func (d *Difference) Increment(i, j, val int) {
	d.diff[i] += val
	if j+1 < len(d.diff) {
		d.diff[j+1] -= val
	}
}

// Result return the result array after a series of operations.
func (d *Difference) Result() []int {
	res := make([]int, len(d.diff))
	res[0] = d.diff[0]
	for i := 1; i < len(d.diff); i++ {
		res[i] = d.diff[i] + res[i-1]
	}
	return res
}
