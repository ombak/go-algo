package strings

// remove extra spaces from a string.
func Clear(s string) string {
	res := ""
	for i := 0; i < len(s); i++ {
		if s[i:i+1] == " " {
			if res != "" && res[len(res)-1:] != " " {
				res += " "
			}
		} else {
			res += s[i : i+1]
		}
	}
	if res != "" && res[len(res)-1:] == " " {
		return res[:len(res)-1]
	}
	return res
}
